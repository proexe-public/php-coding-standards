# Proexe Coding Standard

Proexe coding standard with code style tools in one place for beautiful code.

## Installation
You need to be [authorized by access token](https://tracker.at.proexe.co/articles/PHP-A-9/Generating-access-token)

Add a custom repository to your `composer.json`:

```
"repositories": [
    { "type": "vcs", "url": "https://gitlab.com/proexe-common/php/coding-standard" }
]
```

then

```
composer require --dev "proexe/coding-standard:*"
```

### PhpCS:

Add following rule to Your ruleset.xml

```
<rule ref="vendor/proexe/coding-standard/src/ProexeCodingStandard/ruleset.xml"/>
```


#### Example ruleset.xml for Laravel / Lumen

```xml
<?xml version="1.0"?>
<ruleset name="Proexe Coding Standard">
    <config name="ignore_warnings_on_exit" value="1"/>

    <file>app</file>
    <extensions>php</extensions>
    <exclude-pattern>vendor</exclude-pattern>
    <exclude-pattern>resources</exclude-pattern>
    <exclude-pattern>database/</exclude-pattern>
    <exclude-pattern>storage/</exclude-pattern>
    <exclude-pattern>node_modules/</exclude-pattern>

    <rule ref="vendor/proexe/coding-standard/src/ProexeCodingStandard/ruleset.xml"/>

    <rule ref="Squiz.Classes.ClassFileName.NoMatch">
        <exclude-pattern>*/migrations/*</exclude-pattern>
        <exclude-pattern>*/Migrations/*</exclude-pattern>
    </rule>

    <rule ref="Generic.NamingConventions.CamelCapsFunctionName.NotCamelCaps">
        <exclude-pattern>*/helpers.php</exclude-pattern>
    </rule>

    <rule ref="Squiz.Functions.GlobalFunction.Found">
        <exclude-pattern>*/helpers.php</exclude-pattern>
    </rule>

    <rule ref="SlevomatCodingStandard.Commenting.InlineDocCommentDeclaration.MissingVariable">
        <exclude-pattern>*/routes.php</exclude-pattern>
    </rule>

    <rule ref="PSR1.Classes.ClassDeclaration.MissingNamespace">
        <exclude-pattern>*/Migrations/*</exclude-pattern>
    </rule>
</ruleset>
```

### Php CS Fixer

Create `.php_cs.dist` file and specify with PhpCsFixer\Finder Your project files:

```php
<?php

$finder = PhpCsFixer\Finder::create()
    ->in('app')
    ->notName('routes.php')
    ->name('*.php');

return Proexe\CodingStandard\styles($finder);
```

## Rules

Fixer will automatically add typehints if they are defined in phpDoc block. This can be an unwanted
behavior e.g. when if you're overriding or implementing a parent method which does not have typehints.
In that case you can add `@phpcsSuppress SlevomatCodingStandard.TypeHints.ParameterTypeHint.MissingNativeTypeHint`
to phpDocs:

```
/**
 * @param Request   $request
 * @param Exception $e
 *
 * @return JsonResponse|Response
 *
 * @phpcsSuppress SlevomatCodingStandard.TypeHints.ParameterTypeHint.MissingNativeTypeHint
 */
public function render($request, Exception $e)
{
...
}
```


## Usage

To check for detected standards' violation: 
```
vendor/bin/phpcs --standard=ruleset.xml --tab-width=4 -sp
vendor/bin/php-cs-fixer fix --config=.php_cs.dist --dry-run
```
   
To automatically fix "fixable" errors:
```
vendor/bin/phpcbf --standard=ruleset.xml --tab-width=4 -p
vendor/bin/php-cs-fixer fix --config=.php_cs.dist --show-progress=dots
```
  
## Contributions

If you want to contribute to this repository 
(by suggesting additional rules or by expanding documentation) you can do so by:

- https://gitlab.com/proexe-common/php-coding-standard/-/issues
- create pull requests with proposed changes
- join [#dev-php](https://proexe.slack.com/archives/CL1FF1DTL) channel on slack

    
    
